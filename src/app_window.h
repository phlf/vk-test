#ifndef APP_WINDOW_H
#define APP_WINDOW_H

// clang-format off
#include "vulkan/vulkan.hpp"
#include <GLFW/glfw3.h>
// clang-format on

#include <array>
#include <vector>
#include <optional>

#ifdef NDEBUG
constexpr bool enable_val_layers = false;
#else
constexpr bool enable_val_layers = true;
#endif

const std::array<const char*, 1> val_layers = {"VK_LAYER_KHRONOS_validation"};

class AppWindow {
 public:
  AppWindow();
  ~AppWindow();

  void run();

 private:
  struct QueueFamilyIndices {
    std::optional<uint32_t> graphics;
  };

  void init_win();
  void spdlog_init();
  void glfw_init();

  void                     vk_init();
  std::vector<const char*> vk_get_req_ext();
  bool                     vk_check_val_layer_support();
  void                     vk_new_instance();
  void                     vk_setup_messenger();
  void                     vk_pick_device();
  void                     vk_new_logical_device();
  void                     vk_new_surface();

  static QueueFamilyIndices vk_find_queue_families(
      vk::PhysicalDevice phys_device);

  vk::DebugUtilsMessengerEXT vk_dbg_messenger;
  vk::Instance               vk_instance;
  vk::PhysicalDevice         vk_phys_device;
  vk::Device                 vk_log_device;
  vk::SurfaceKHR             vk_surface;

  GLFWwindow* window;
  int         initialized;

  const uint32_t init_width;
  const uint32_t init_height;
};

#endif  // APP_WINDOW_H
