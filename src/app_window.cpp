#include "app_window.h"

#include <string>

#include "spdlog/spdlog.h"

VULKAN_HPP_DEFAULT_DISPATCH_LOADER_DYNAMIC_STORAGE

static VKAPI_ATTR vk::Bool32 VKAPI_CALL
                  debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT /*message_severity*/,
                                 VkDebugUtilsMessageTypeFlagsEXT /*message_type*/,
                                 const VkDebugUtilsMessengerCallbackDataEXT* cb_data,
                                 void* /*user_data*/) {
  SPDLOG_DEBUG("validation layer: {}", cb_data->pMessage);

  return VK_FALSE;
}

static vk::DebugUtilsMessengerCreateInfoEXT get_debug_msg_create_info() {
  constexpr auto message_severity =
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

  constexpr auto message_type = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                                VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                                VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

  return {{},
          static_cast<vk::DebugUtilsMessageSeverityFlagsEXT>(message_severity),
          static_cast<vk::DebugUtilsMessageTypeFlagsEXT>(message_type),
          debug_callback,
          nullptr};
}

AppWindow::AppWindow() : window(nullptr), init_width(800), init_height(600) {
  spdlog_init();
  glfw_init();
  vk_init();
  init_win();
}

AppWindow::~AppWindow() {
  vk_log_device.destroy();
  if constexpr (enable_val_layers) {
    vk_instance.destroy(vk_dbg_messenger);
  }
  vk_instance.destroy();

  if (window != nullptr) {
    glfwDestroyWindow(window);
  }
  if (initialized == GLFW_TRUE) {
    glfwTerminate();
  }
}

void AppWindow::glfw_init() {
  initialized = glfwInit();

  if (glfwVulkanSupported() == GLFW_FALSE) {
    spdlog::error("Vulkan not supported");
  }
}

void AppWindow::init_win() {
  if (initialized == GLFW_TRUE) {
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    window =
        glfwCreateWindow(init_width, init_height, "Vulk", nullptr, nullptr);
  }
}

void AppWindow::spdlog_init() {
  spdlog::set_pattern("[%E,%f][%l][%!]: %v");
  SPDLOG_INFO("Initializing spdlog");
}

void AppWindow::vk_init() {
  if (initialized == GLFW_TRUE) {
    // Necessary for resolving calls to EXT functions
    vk::DynamicLoader dl;
    const auto        vkGetInstanceProcAddr =
        dl.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr");

    if (vkGetInstanceProcAddr != nullptr) {
      VULKAN_HPP_DEFAULT_DISPATCHER.init(vkGetInstanceProcAddr);
      vk_new_instance();
      if constexpr (enable_val_layers) {
        vk_setup_messenger();
      }
      vk_pick_device();
      vk_new_logical_device();

    } else {
      SPDLOG_ERROR(
          "Failed to load \"vkGetInstanceProcAddr\" from libvulkan.so");
    }
  }
}

std::vector<const char*> AppWindow::vk_get_req_ext() {
  // Retrieve VLK extensions required by glfw
  uint32_t     glfw_ext_count = 0;
  const char** glfw_req_ext =
      glfwGetRequiredInstanceExtensions(&glfw_ext_count);

  std::vector<const char*> extensions(glfw_req_ext,
                                      glfw_req_ext + glfw_ext_count);
  if constexpr (enable_val_layers) {
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
  }
  return extensions;
}

bool AppWindow::vk_check_val_layer_support() {
  std::vector<vk::LayerProperties> available_layers =
      vk::enumerateInstanceLayerProperties();

  for (auto layer_name : val_layers) {
    const auto matching_layer = std::find_if(
        std::cbegin(available_layers),
        std::cend(available_layers),
        [layer_name](const auto& layer_properties) {
          return std::string(layer_name) == layer_properties.layerName;
        });

    if (matching_layer == std::cend(available_layers)) {
      SPDLOG_WARN("Unable to find {}", layer_name);
    }
  }

  return true;
}

void AppWindow::vk_new_instance() {
  vk::ApplicationInfo app_info("Hello Mr. Triangle",
                               VK_MAKE_VERSION(1, 0, 0),
                               "Void Engine",
                               VK_MAKE_VERSION(1, 0, 0),
                               VK_API_VERSION_1_2);

  // Retrieve VLK extensions supported by the platform
  std::vector<vk::ExtensionProperties> supp_ext =
      vk::enumerateInstanceExtensionProperties();

  SPDLOG_INFO("Available extensions:");
  for (const auto& ext : supp_ext) {
    SPDLOG_INFO("{}", ext.extensionName);
  }

  auto               val_layers_count         = 0;
  const char* const* enabled_val_layers_names = nullptr;
  if constexpr (enable_val_layers) {
    val_layers_count         = static_cast<uint32_t>(val_layers.size());
    enabled_val_layers_names = val_layers.data();
  }

  auto extensions = vk_get_req_ext();

  vk::InstanceCreateInfo create_info({},
                                     &app_info,
                                     val_layers_count,
                                     enabled_val_layers_names,
                                     extensions.size(),
                                     extensions.data());

  const auto debug_info = get_debug_msg_create_info();
  if constexpr (enable_val_layers) {
    create_info.setPNext(&debug_info);
  }

  try {
    vk_instance = vk::createInstance(create_info);
    SPDLOG_INFO("Loading vulkan instance related functions");
    VULKAN_HPP_DEFAULT_DISPATCHER.init(vk_instance);
  } catch (vk::SystemError& e) {
    SPDLOG_ERROR("Failed to create VLK instance: {}", e.what());
  }
}

void AppWindow::vk_setup_messenger() {
  vk_dbg_messenger =
      vk_instance.createDebugUtilsMessengerEXT(get_debug_msg_create_info(),
                                               nullptr);
}

void AppWindow::vk_pick_device() {
  const auto devices = vk_instance.enumeratePhysicalDevices();

  const auto found_device = std::find_if(
      std::cbegin(devices),
      std::cend(devices),
      [](const vk::PhysicalDevice& phys_device) {
        if (vk_find_queue_families(phys_device).graphics.has_value()) {
          return true;
        }
        return false;
      });

  if (found_device == std::cend(devices)) {
    SPDLOG_ERROR("failed to find a suitable GPU");
  } else {
    const auto device_name =
        std::string(found_device->getProperties().deviceName.data());
    SPDLOG_INFO("Using {} as Vulkan physical device", device_name);
    vk_phys_device = *found_device;
  }
}

void AppWindow::vk_new_logical_device() {
  static constexpr auto priority          = 1.0f;
  static constexpr auto count             = 1;
  static constexpr auto create_info_count = 1;

  const auto indices = vk_find_queue_families(vk_phys_device);

  vk::DeviceQueueCreateInfo  queue_create_info({},
                                              *indices.graphics,
                                              count,
                                              &priority);
  vk::PhysicalDeviceFeatures device_features;

  vk::DeviceCreateInfo create_info({},
                                   create_info_count,
                                   &queue_create_info,
                                   {},
                                   {},
                                   {},
                                   {},
                                   &device_features);
  try {
    vk_log_device = vk_phys_device.createDevice(create_info);
  } catch (vk::SystemError& e) {
    SPDLOG_ERROR("unable to create logical device: {}", e.what());
  }
}

void AppWindow::vk_new_surface() {
  VkSurfaceKHR surface = static_cast<VkSurfaceKHR>(vk_surface);

  if (glfwCreateWindowSurface(static_cast<VkInstance>(vk_instance),
                              window,
                              nullptr,
                              &surface) != VK_SUCCESS) {
    SPDLOG_ERROR("failed to create window surface!");
  }
}

AppWindow::QueueFamilyIndices AppWindow::vk_find_queue_families(
    vk::PhysicalDevice phys_device) {
  QueueFamilyIndices indices;

  const auto queue_family_properties = phys_device.getQueueFamilyProperties();

  // Looking for graphics capable queue
  for (size_t i = 0; i < queue_family_properties.size(); ++i) {
    if (queue_family_properties[i].queueFlags & vk::QueueFlagBits::eGraphics) {
      indices.graphics = i;
      break;
    }
  }

  return indices;
}

void AppWindow::run() {
  if (window != nullptr) {
    //  while (!glfwWindowShouldClose(window)) {
    //    glfwPollEvents();
    //  }
  }
}
