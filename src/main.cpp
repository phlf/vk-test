#include <chrono>
#include <thread>

#include "spdlog/spdlog.h"

#include "app_window.h"

int main() {
  AppWindow app;

  app.run();

  return 0;
}
