cmake_minimum_required(VERSION 3.17)

project(vk LANGUAGES CXX)

include(FetchContent)
FetchContent_Declare(
    vulkan
    GIT_REPOSITORY https://github.com/KhronosGroup/Vulkan-Headers.git
    GIT_TAG        09531f27933bf04bffde9074acb302e026e8f181
    GIT_SHALLOW    TRUE
    GIT_PROGRESS   TRUE
    SOURCE_DIR     ${CMAKE_CURRENT_SOURCE_DIR}/lib/vulkan
)
FetchContent_MakeAvailable(vulkan)

FetchContent_Declare(
    spdlog
    GIT_REPOSITORY https://github.com/gabime/spdlog.git
    GIT_TAG        v1.6.0
    GIT_SHALLOW    TRUE
    GIT_PROGRESS   TRUE
    SOURCE_DIR     ${CMAKE_CURRENT_SOURCE_DIR}/lib/spdlog
)
FetchContent_MakeAvailable(spdlog)

FetchContent_Declare(
    glm
    GIT_REPOSITORY https://github.com/g-truc/glm.git
    GIT_TAG        0.9.9.8
    GIT_SHALLOW    TRUE
    GIT_PROGRESS   TRUE
    SOURCE_DIR     ${CMAKE_CURRENT_SOURCE_DIR}/lib/glm
)
FetchContent_MakeAvailable(glm)

find_package(glfw3 REQUIRED)
find_library(libvulkan vulkan)

if(NOT libvulkan)
    message(FATAL_ERROR "vulkan shared library was not found")
else()
    message("vulkan shared library found at: ${libvulkan}")
endif()

find_library(dl dl)
if(NOT dl)
    message(FATAL_ERROR "dl shared library was not found")
else()
    message("dl shared library found at: ${dl}")
endif()


add_executable(vk src/main.cpp
                  src/app_window.cpp)

set_target_properties(vk   PROPERTIES CXX_STANDARD           17
                                      CXX_EXTENSIONS        OFF
                                      CXX_STANDARD_REQUIRED  ON
)
target_compile_definitions(vk PRIVATE GLM_FORCE_RADIAN
                                      GLM_FORCE_DEPTH_ZERO_TO_ONE
                                      SPDLOG_FUNCTION=__PRETTY_FUNCTION__
                                      VULKAN_HPP_DISPATCH_LOADER_DYNAMIC=1
)

target_link_libraries(vk PRIVATE Vulkan::Headers
                                 #Not necessary as dynamically loading the library
                                 #${libvulkan}
                                 dl
                                 glfw
                                 glm::glm
                                 spdlog::spdlog
)
